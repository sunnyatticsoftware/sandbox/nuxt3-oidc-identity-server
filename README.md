# nuxt3-oidc-identity-server

View this original repo for more details
https://github.com/aborn/nuxt-openid-connect

## How to Run
1. Run Identity Server in https://localhost:5000 with the following client registration
```
{
        "ClientId": "rubiko",
        "RequirePkce": false,
        "ClientSecrets": [
          {
            "Value": "rubiko-secret",
            "Description": "The secret for rubiko UI"
          }
        ],
        "AllowedGrantTypes": ["authorization_code"],
        "AllowOfflineAccess": true,
        "AllowedScopes": ["openid", "profile", "rubiko-api"],
        "RedirectUris": [
          "http://localhost:3000/oidc/callback?redirect=/secure"
        ],
        "PostLogoutRedirectUris ": [
          "http://localhost:3000/oidc/foo"
        ]
      }
```
2. Add an environment variable `NODE_TLS_REJECT_UNAUTHORIZED` set to `0`. Otherwise the Authority will be rejected due to self signed certificate
3. Adapt the settings in the nuxtJs application (or leave as they are)
4. Run
```
npm install
npm run dev:prepare
npm run dev
```